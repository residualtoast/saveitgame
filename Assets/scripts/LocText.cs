﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LocText
{
    public static Dictionary<string, string> allText = new Dictionary<string, string>() {
        //intro
        { "INTRO", "You must protect the TREE OF LIFE. Press arrow key to move."},
        //movement
        { "NORTH", "You step NORTH."},
        { "SOUTH", "You step SOUTH."},
        { "EAST", "You step EAST."},
        { "WEST", "You step WEST."},
        //spawns
        { "SPAWN_CULTIST", "A blind, crazed cultist with an axe appears in the west."},
        { "SPAWN_BOULDER", "You hear a CRASH as cultist release a boulder from the north."},
        { "SPAWN_FIRE", "You see a cultist start a fire in the northeast grasslands."},
        { "SPAWN_VINES", "A cultist has planted choke vines in the southeast."},
        { "SPAWN_INSECTS", "You see a swarm of hungry locusts approach from the southwest."},
        //find
        { "FIND_PUMP", "There is a pump sourced by the river." },
        { "FIND_LADDER", "There is a ladder against the side of the building. You take it with you." },
        { "FIND_HOSE", "You come across a garden hose. You take it." },
        { "FIND_SHOVEL", "You pick up a garden shovel. Maybe it will be useful somehow." },
        { "FIND_PESTICIDE", "You see a spray can labelled PESTICIDE. You take it with you." },
        { "FIND_TORCH", "There is an unlit tiki torch in the corner. You pick it up." },
        { "FIND_RED_SCROLL", "There is a scroll with a RED seal" },
        { "FIND_PURPLE_SCROLL", "There is a scroll with a PURPLE seal" },
        { "FIND_GREEN_SCROLL", "There is a scroll with a GREEN seal" },
        //player actions
        { "BAD_ACTION", "Nothing seems to happen." },
        { "USE_UNLIT_TORCH", "You reach out toward the fire and carefully light the torch."},
        { "USE_LIT_TORCH", "You reach out and touch the flame of the torch to the vines, lighting them on fire."},
        { "USE_SHOVEL", "You use the shovel to dig an embankment. It could be used to deflect something."},
        { "USE_PUMP", "You use the pump. Water gushes from the spigot onto the ground."},
        { "USE_PUMP_AND_HOSE", "You use the pump and water rushes through the hose to the northwest."},
        { "USE_HOSE", "You connect the hose to the pump and point it northwest."},
        { "USE_LADDER", "You use the ladder to create a dubious looking bridge across the river."},
        //scrolls
        { "RED_SCROLL", "You come across a scroll with a RED seal. It reads 'The key code is 9-3-5'" },
        { "PURPLE_SCROLL", "You come across a scroll with a PURPLE seal. It reads 'The key code is 2-7-1'" },
        { "GREEN_SCROLL", "You come across a scroll with a GREEN seal. It reads 'The key code is 3-0-8'" },
        { "OPEN_DOOR", "You try the code from the {0} and the {1} door opens" },
        { "OPEN_DOOR_FAILURE", "You see a code wheel on the {0} door, but you don't know what to enter" },
        //deaths
        { "PLAYER_DEATH_BOULDER", "You have been crushed by a boulder, I will use what power I have to restore you. (Press Any Key)"},
        { "PLAYER_DEATH_FIRE", "You have been consumed by flames, I will use what power I have to restore you. (Press Any Key)" },
        { "PLAYER_DEATH_CULTIST", "You have been murdered by a cultist, I will use what power I have to restore you. (Press Any Key)" },
        { "TREE_DEATH_BOULDER", "The TREE OF TIME was destroyed by a boulder. (Press Any Key)" },
        { "TREE_DEATH_FIRE", "The TREE OF TIME was ravaged by fire. (Press Any Key)" },
        { "TREE_DEATH_CULTIST", "The TREE OF TIME was cut down by a berserking cultist. (Press Any Key)" },
        { "TREE_DEATH_VINES", "The TREE OF TIME was choked by vines. (Press Any Key)" },
        { "TREE_DEATH_INSECTS", "The TREE OF TIME was consumed by locusts. (Press Any Key)" },
        { "INSECT_DEATH", "You spray the locusts with the can of pesticide. They slow for a few moments, then fall to the ground dead." },
        { "FIRE_DEATH", "The fire has been put out" },
        { "VINES_DEATH", "The vines have been fully destroyed by fire" },
        { "DISPLAY_ITEMS", "Press the key to use the item: \n" },
        { "INPUT_DIRECTION_SHOVEL", "Press the arrow key to choose the direction of your embankment" },
        { "WIN_GAME", "The tree is safe from all that would try to harm it, and you can rest.\n\n YOU WIN!" },
    };

    public static string GetLocForKey(string key) {
        if(allText.ContainsKey(key)) {
            return allText[key];
        } else {
            UnityEngine.Debug.LogError($"Failed to find key in loc {key}");
            return "";
        }
    }
}
