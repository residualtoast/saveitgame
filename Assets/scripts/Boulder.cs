﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : Entity
{
    // Start is called before the first frame update
    protected override void Start() {
        base.Start();
        movementDirection = Direction.Down;
        isSolid = true;
    }

    public override void OnCollision(Entity collidingEntity)
    {
        if (collidingEntity is Character) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("PLAYER_DEATH_BOULDER"));
            MapManager.Instance.failState = true;
        }

        if (collidingEntity is BigTree) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("TREE_DEATH_BOULDER"));
            MapManager.Instance.failState = true;
        }

        if (collidingEntity is SmallTree) {
            MapManager.Instance.Remove(collidingEntity);
            collidingEntity.currentNode.ChangeTileType(MapNode.NodeType.Dirt);
        }
    }

}
