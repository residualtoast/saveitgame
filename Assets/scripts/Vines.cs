﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vines : Entity
{
    // spawn vines in all directions
    public override void OnMoveComplete()
    {
        if (speedTicker < speed) {
            base.OnMoveComplete();
            return;
        }
        var xPos = currentNode.position.x;
        var yPos = currentNode.position.y;

        var upPos = new Vector2Int(xPos, yPos - 1);
        var downPos = new Vector2Int(xPos, yPos + 1);
        var leftPos = new Vector2Int(xPos - 1, yPos);
        var rightPos = new Vector2Int(xPos + 1, yPos);

        MapManager.Instance.Spawn("Vines", upPos);
        MapManager.Instance.Spawn("Vines", downPos);
        MapManager.Instance.Spawn("Vines", leftPos);
        MapManager.Instance.Spawn("Vines", rightPos);

        base.OnMoveComplete();
    }

    public override void OnCollision(Entity collidingEntity)
    {

        if (collidingEntity is BigTree)
        {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("TREE_DEATH_VINES"));
            MapManager.Instance.failState = true;
        }
    }
}
