﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Embankment : Entity
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        if (movementDirection == Direction.Left) {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        } else if (movementDirection == Direction.Right) {
            transform.rotation = Quaternion.Euler(0, 0, 270);
        } else if (movementDirection == Direction.Down) {
            transform.rotation = Quaternion.Euler(0, 0, 180);
        }
    }

    public override void OnCollision(Entity collidingEntity) {
        if (collidingEntity is Boulder) {
            collidingEntity.movementDirection = this.movementDirection;
        }
    }
}
