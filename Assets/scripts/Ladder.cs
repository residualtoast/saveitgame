﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ladder : InventoryItem
{
    public Sprite sidewaysLadder;
    public bool isSideways;

    public override void OnCollision(Entity collidingEntity) {
        if (!isSideways) {
            base.OnCollision(collidingEntity);
        }
    }

    public override MapManager.InputMode SelectItem() {
        
        var currentPosition = MapManager.Instance.character.currentNode.position;
        var rightNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x + 1, currentPosition.y));
        if (rightNode.nodeType == MapNode.NodeType.River) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_LADDER"));
            MapManager.Instance.inventory.Remove(this);
            MapManager.Instance.Remove(this, true);
            var newEntity = MapManager.Instance.Spawn("Ladder", rightNode.position) as Ladder;
            newEntity.GetComponent<Image>().sprite = sidewaysLadder;
            newEntity.isSideways = true;
        } else {
            return base.SelectItem();
        }
        
        
        return MapManager.InputMode.Movement;
    }
}
