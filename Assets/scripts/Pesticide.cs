﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pesticide : InventoryItem
{
    public override MapManager.InputMode SelectItem() {
        var currentPosition = MapManager.Instance.character.currentNode.position;
        var rightNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x + 1, currentPosition.y));
        var leftNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x - 1, currentPosition.y));
        var upNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x, currentPosition.y - 1));
        var downNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x, currentPosition.y + 1));

        if (rightNode != null && rightNode.entities.Exists(x => x is Insects)) {
            MapManager.Instance.Remove(rightNode.entities.Find(x => x is Insects), true);
        }
        if (leftNode != null && leftNode.entities.Exists(x => x is Insects)) {
            MapManager.Instance.Remove(leftNode.entities.Find(x => x is Insects), true);
        }

        if (upNode != null && upNode.entities.Exists(x => x is Insects)) {
            MapManager.Instance.Remove(upNode.entities.Find(x => x is Insects), true);
        }

        if (downNode != null && downNode.entities.Exists(x => x is Insects)) {
            MapManager.Instance.Remove(downNode.entities.Find(x => x is Insects), true);
        }

        return MapManager.InputMode.Movement;
    }
}
