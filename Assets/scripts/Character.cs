﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Entity
{
    // Start is called before the first frame update
    protected override void Start() {
        base.Start();
        isSolid = true;
    }

    override public void OnMoveComplete() {
        //display text for direction moved
    }
}
