﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Entity
{
    protected override void Start() {
        currentNode.ChangeTileType(MapNode.NodeType.Dirt);
    }

    // spawn fire in all directions
    public override void OnMoveComplete()
    {
        if (speedTicker < speed) {
            base.OnMoveComplete();
            return;
        }
        var xPos = currentNode.position.x;
        var yPos = currentNode.position.y;
        
        var upPos = new Vector2Int(xPos, yPos - 1);
        var downPos = new Vector2Int(xPos, yPos + 1);
        var leftPos = new Vector2Int(xPos - 1, yPos);
        var rightPos = new Vector2Int(xPos + 1, yPos);

        SpawnFire(upPos);
        SpawnFire(downPos);
        SpawnFire(leftPos);
        SpawnFire(rightPos);
        
        base.OnMoveComplete();
    }

    private void SpawnFire(Vector2Int position) {
        MapManager.Instance.Spawn("Fire", position);
    }

    public override void OnCollision(Entity collidingEntity) {
        if (collidingEntity is Vines) {
            MapManager.Instance.Remove(collidingEntity);
            speed = 1;
            lifetime = 2;
        }

        if (collidingEntity is Character) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("DEATH_FIRE"));
            MapManager.Instance.failState = true;
        }

        if (collidingEntity is BigTree)
        {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("TREE_DEATH_FIRE"));
            MapManager.Instance.failState = true;
        }
    }
}
