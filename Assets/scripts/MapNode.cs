﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapNode : MonoBehaviour
{
    public enum NodeType
    {
        Dirt=0,
        Grass=1,        
        River=2,
        Wall=3,
        Floor=4,
        Tree=6,
        Mix=7,
        Door_Purple = 8,
        Door_Red = 9,
        Door_Green = 10
    }
    
    public Vector2Int position;
    public List<Entity> entities = new List<Entity>();

    public NodeType nodeType;

    // Start is called before the first frame update
    void Start()
    {
        if (this.nodeType == NodeType.Tree) {
            MapManager.Instance.Spawn("SmallTree", position);
        }
    }

    public bool MoveEntityToNode(Entity entity) {
        if (IsBlocking(entity)) {
            return false;
        }

        entity.gameObject.transform.SetParent(this.transform, false);
        entity.currentNode?.entities.Remove(entity);
        entity.currentNode = this;
        entities.Add(entity);

        foreach(var otherEntity in entities) {
            if (entity == otherEntity) {
                continue;
            }
            entity.OnCollision(otherEntity);
            otherEntity.OnCollision(entity);
        }

        return true;
    }

    public void ChangeTileType(NodeType newType) {
        var newSprite = Resources.Load<GameObject>($"MapNodes/{newType.ToString()}").GetComponent<Image>().sprite;
        this.GetComponent<Image>().sprite = newSprite;
        nodeType = newType;
    }

    private bool IsBlocking(Entity entity) {

        if (nodeType == NodeType.River){
            if (!(entity is Ladder) && !(entity is Character && entities.Exists(x => x is Ladder))) {
                return true;
            }
        }

        if (nodeType == NodeType.Wall || (nodeType == NodeType.Tree && !(entity is SmallTree))) {
            return true;
        }

        if (nodeType == NodeType.Door_Purple || nodeType == NodeType.Door_Red || nodeType == NodeType.Door_Green) {//TODO make door be locked
            if (!(entity is Character)) {
                return true;
            }

            var colorName = nodeType.ToString().Split('_')[1];
            var colorScrollName = colorName + "_Scroll";
            if (!MapManager.Instance.inventory.Exists(x=>x.GetName() == colorScrollName )) {
                var doorText = LocText.GetLocForKey("OPEN_DOOR_FAILURE");
                doorText = string.Format(doorText, colorName);
                MapManager.Instance.DisplayText(doorText);
                return true;
            } else {
                var doorText = LocText.GetLocForKey("OPEN_DOOR");
                doorText = string.Format(doorText, colorScrollName.Replace("_", " "), colorName);
                MapManager.Instance.DisplayText(doorText);
            }
            

        }

        if (entity is Vines && entities.Find(x=>x is Fire)){ 
            return true; 
        }

        if (entity is Vines && !(nodeType == NodeType.Dirt)) {
            return true;
        }

        if (entity is Fire && entities.Exists(x=>x is FlowingWater)) {
            return true;
        }

        if (entity is Fire && nodeType == NodeType.Mix) {
            return true;
        }

        if (nodeType == NodeType.Dirt && entity is Fire && !entities.Exists(x=>x is Vines)) {
            return true;
        }

        return false;
    }
}
