﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowingWater : Entity
{
    public override void OnMoveComplete() {
        if (speedTicker < speed) {
            base.OnMoveComplete();
            return;
        }
        var xPos = currentNode.position.x;
        var yPos = currentNode.position.y;

        var upPos = new Vector2Int(xPos, yPos - 1);
        
        var leftPos = new Vector2Int(xPos - 1, yPos);
        

        MapManager.Instance.Spawn("FlowingWater", upPos);
        MapManager.Instance.Spawn("FlowingWater", leftPos);
        base.OnMoveComplete();
    }

    public override void OnCollision(Entity collidingEntity) {
        if (collidingEntity is Fire) {
            MapManager.Instance.Remove(collidingEntity);
        }
    }

}
