﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#pragma warning disable 0649
public class MapManager : MonoBehaviour
{
    private static MapManager instance = null;

    public List<Image> MapImages;

    public static MapManager Instance {
        get {
            return instance;
        }
    }

    private void Awake() {
        instance = this;
    }

    private int[,] MapGrid = {
        { 0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,1,1,1, },
        { 0,3,3,3,3,0,0,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1, },
        { 0,3,4,4,3,3,3,3,3,3,3,0,0,1,1,1,0,1,1,1,0,0,0,1,1,1,0,1,1,1, },
        { 0,3,4,4,8,4,4,4,4,4,3,0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,0,1,1,1, },
        { 0,3,3,3,3,4,4,4,4,4,3,0,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1, },
        { 0,0,0,0,3,4,4,4,4,4,3,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1, },
        { 0,1,1,1,3,3,3,3,3,9,3,0,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,2,2,2, },
        { 0,0,0,1,1,1,0,0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,2,1,1, },
        { 0,0,1,1,1,7,7,1,1,1,7,7,1,1,1,0,0,1,1,1,0,1,1,1,2,2,2,2,1,1, },
        { 0,1,1,1,7,7,7,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,0,2,1,1,1,1,1, },
        { 0,0,0,0,0,1,1,1,7,7,1,1,1,1,1,1,1,1,1,1,1,1,0,0,2,1,1,1,1,1, },
        { 0,0,1,1,1,1,1,1,7,7,1,1,1,1,1,1,1,1,1,0,0,0,0,0,2,1,1,1,1,1, },
        { 0,0,0,1,1,1,7,6,7,1,1,1,0,0,1,1,1,0,0,0,0,0,7,0,2,1,1,1,1,1, },
        { 0,0,0,6,7,7,7,1,1,1,1,6,0,0,0,0,0,0,0,0,0,0,0,0,2,1,1,1,1,1, },
        { 0,1,1,1,7,6,1,1,1,7,1,1,1,0,0,0,0,0,0,7,0,0,7,0,2,1,1,1,1,1, },
        { 0,0,0,0,0,1,1,1,7,7,6,1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,1, },
        { 0,0,0,0,0,0,6,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,2,1, },
        { 0,6,0,1,1,1,0,0,6,1,1,1,0,0,0,0,0,0,0,7,0,0,0,0,0,0,7,0,2,1, },
        { 0,0,0,0,6,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1, },
        { 0,0,1,1,1,0,0,1,1,1,6,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,7,0,2,1, },
        { 1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1, },
        { 1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,2,2, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,7,0,0,7,0,3,3,10,3,3,3,3,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,3,4,4,4,4,4,4,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,7,7,7,0,0,0,3,4,4,4,4,4,4,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,3,4,4,4,4,4,4,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,3,4,4,4,4,4,4,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,0,7,0,0,0,0,3,3,3,3,3,3,3,3,0,0,0,0, },
        { 7,7,7,7,7,7,7,7,7,7,0,0,7,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0, },
    };

    public struct SpawnData
    {
        public int spawnTurn;
        public string spawnID;
        public Vector2Int spawnPosition;

        public SpawnData(int spawnTurn, string spawnID, Vector2Int spawnPosition) {
            this.spawnTurn = spawnTurn;
            this.spawnID = spawnID;
            this.spawnPosition = spawnPosition;
        }
    }

    Vector2Int characterStartPosition = new Vector2Int(16, 15);

    private List<SpawnData> spawnData = new List<SpawnData>() {
        new SpawnData(1, "The_Tree", new Vector2Int(14, 15)),
        new SpawnData(1, "Hose", new Vector2Int(27, 11)),
        new SpawnData(1, "Pump", new Vector2Int(23, 9)),
        new SpawnData(1, "Shovel", new Vector2Int(22, 25)),
        new SpawnData(1, "Pesticide", new Vector2Int(8, 3)),
        new SpawnData(1, "Torch", new Vector2Int(2, 2)),
        new SpawnData(1, "Green_Scroll", new Vector2Int(11, 15)),
        new SpawnData(1, "Red_Scroll", new Vector2Int(16, 6)),
        new SpawnData(1, "Purple_Scroll", new Vector2Int(23, 18)),
        new SpawnData(1, "Ladder", new Vector2Int(17, 24)),
        new SpawnData(45, "Cultist", new Vector2Int(0, 14)),
        new SpawnData(115, "Insects", new Vector2Int(5, 29)),
        new SpawnData(17, "Boulder", new Vector2Int(14, 0)),
        new SpawnData(112, "Fire", new Vector2Int(26, 4)),
        new SpawnData(75, "Vines", new Vector2Int(26, 29))
    };

    public int mapWidth;
    public int mapHeight;

    private bool isMovementLoop = false;

    private List<List<MapNode>> mapNodes = new List<List<MapNode>>();
    private List<Entity> allEntities = new List<Entity>();
    public Character character;

    private List<Tuple<string, Vector2Int>> pendingSpawns = new List<Tuple<string, Vector2Int>>();
    private List<Entity> pendingDeletions = new List<Entity>();

    public List<InventoryItem> inventory = new List<InventoryItem>();
    public RectTransform InventoryPanel;

    private int turnCount = 1;

    

    [SerializeField] private TextMeshProUGUI dialogText;
    public bool failState;

    // Start is called before the first frame update
    void Start()
    {
        for (var i = 0; i < mapWidth; i++) {
            mapNodes.Add(new List<MapNode>());
        }

        for(var yPos = 0; yPos < mapHeight; yPos++) {
            for (var xPos = 0; xPos < mapWidth; xPos++) {
                var tileType = (MapNode.NodeType)MapGrid[yPos, xPos];
                var newMapTile = GameObject.Instantiate(Resources.Load<GameObject>("MapNodes/" + tileType.ToString()), this.gameObject.transform);
                var newNodeComponent = newMapTile.GetComponent<MapNode>();
                mapNodes[xPos].Add(newNodeComponent);
                newNodeComponent.position = new Vector2Int(xPos, yPos);
                newNodeComponent.nodeType = tileType;
                //TODO make this load maptiles and stuff
            }
        }

        dialogText.text = LocText.GetLocForKey("INTRO") + "\n\n";

        character = Spawn("Character", characterStartPosition, true) as Character;


        SpawnForTurn();
    }

    public MapNode GetNodeAtPosition(Vector2Int position) {

        if (position.x > mapNodes.Count || position.y > mapNodes[0].Count) {
            return null;
        }
        return mapNodes[position.x][position.y];
    }

    public void DisplayText(string displayString) {
        dialogText.text += displayString + "\n\n";
    }

    public Entity Spawn(string resourceName, Vector2Int startPosition, bool isFirstSpawn = false) {

        if (startPosition.x >= mapWidth || startPosition.x < 0 || startPosition.y >= mapHeight || startPosition.y < 0) {
            return null;
        }
        if(isMovementLoop) {
            pendingSpawns.Add(new Tuple<string, Vector2Int>(resourceName, startPosition));
            return null;
        }

        var mapNodeTarget = mapNodes[startPosition.x][startPosition.y];
        if (mapNodeTarget.entities.Find(x=>resourceName == x.GetName())) {//no duplicate things in a cell
            return null;
        }

        var newEntity = GameObject.Instantiate(Resources.Load<GameObject>(resourceName)).GetComponent<Entity>();
        if (isFirstSpawn) {
            newEntity.isFirstSpawn = true;
        }

        if (mapNodeTarget.MoveEntityToNode(newEntity)) {
            allEntities.Add(newEntity);

            newEntity.alreadyMoved = true;
            return newEntity;
        } else {
            Destroy(newEntity.gameObject);
            return null;
        }
    }

    public enum InputMode
    {
        Movement,
        ItemSelect,
        ItemDirection
    }

    public InputMode currentInputMode = InputMode.Movement;
    public InventoryItem selectedItem = null;

    public void DisplayItemSelections() {
        var displayString = LocText.GetLocForKey("DISPLAY_ITEMS");
        for (var i = 0; i < inventory.Count; i++) {
            var displayName = 
            displayString += $" {i + 1}: {inventory[i].GetName().Replace("_", " ")}\n";
        }

        DisplayText(displayString);
    }

    public void Update() {

        if (failState && Input.anyKeyDown) {
            SceneManager.LoadScene("MainScene");
            return;
        }

        if (currentInputMode == InputMode.Movement) { 
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                character.movementDirection = Entity.Direction.Up;
            } else if (Input.GetKeyDown(KeyCode.DownArrow)) {
                character.movementDirection = Entity.Direction.Down;
            } else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                character.movementDirection = Entity.Direction.Left;
            } else if (Input.GetKeyDown(KeyCode.RightArrow)) {
                character.movementDirection = Entity.Direction.Right;
            } else if (Input.GetKeyDown(KeyCode.Space) && inventory.Count > 0) {
                currentInputMode = InputMode.ItemSelect;
                //TODO display text for which item
                DisplayItemSelections();
            }
        }

        if (currentInputMode == InputMode.ItemSelect) {
            for (int number = 1; number <= 9; number++) {
                if (Input.GetKeyDown(number.ToString()) && number-1 < inventory.Count) {
                    selectedItem = inventory[number - 1];
                    currentInputMode = selectedItem.SelectItem();
                    if (currentInputMode == InputMode.Movement) {
                        selectedItem = null;
                    }
                }
            }
        }

        if (currentInputMode == InputMode.ItemDirection) {
            var itemDirection = Entity.Direction.None;
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                itemDirection = Entity.Direction.Up;
            } else if (Input.GetKeyDown(KeyCode.DownArrow)) {
                itemDirection = Entity.Direction.Down;
            } else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                itemDirection = Entity.Direction.Left;
            } else if (Input.GetKeyDown(KeyCode.RightArrow)) {
                itemDirection = Entity.Direction.Right;
            }

            if (itemDirection != Entity.Direction.None) {
                selectedItem.SelectDirection(itemDirection);
                currentInputMode = InputMode.Movement;
                selectedItem = null;
            }

        }



        if (character.movementDirection != Entity.Direction.None) {
            StartTurn();
        }
        


    }

    public void StartTurn() {
        

        foreach (var entity in allEntities) {
            entity.alreadyMoved = false;
        }

        isMovementLoop = true;

        foreach (var entity in allEntities) {
            Move(entity);
        }
        character.movementDirection = Entity.Direction.None;

        isMovementLoop = false;

        foreach (var entity in allEntities) {
            entity.alreadyMoved = false;
        }

        while (pendingSpawns.Count > 0) {
            var spawnData = pendingSpawns[0];
            pendingSpawns.RemoveAt(0);
            Spawn(spawnData.Item1, spawnData.Item2);
        }

        ClearPendingDeletions();

        turnCount++;

        SpawnForTurn();

        CheckWinGame();
    }

    public GameObject youWinText;

    private void CheckWinGame() {
        if (turnCount > spawnData.Last().spawnTurn && !allEntities.Exists(x=>x.isHazard)) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("WIN_GAME"));
            youWinText.SetActive(true);
        } 
    }

    private void ClearPendingDeletions() {
        while (pendingDeletions.Count > 0) {
            allEntities.Remove(pendingDeletions[0]);
            Destroy(pendingDeletions[0].gameObject);
            pendingDeletions.RemoveAt(0);
        }

    }

    private void SpawnForTurn() {
        if (!spawnData.Exists(x => x.spawnTurn == turnCount)) {
            return;
        }
        
        var allNextSpawnData = spawnData.FindAll(x => x.spawnTurn == turnCount);
        foreach (var nextSpawnData in allNextSpawnData) {
            Spawn(nextSpawnData.spawnID, nextSpawnData.spawnPosition, true);
        }
    }

    public void Move(Entity entity) {
        if (!entity.doesMove) {
            entity.OnMoveComplete();
        }

        if (!entity.alreadyMoved && entity.doesMove && !pendingDeletions.Contains(entity)) {
            AttemptValidMove(entity, entity.GetNextPosition());
        }
    }

    private bool AttemptValidMove(Entity entity, Vector2Int newPosition) {
        var curNode = mapNodes[newPosition.x][newPosition.y];
        var hasMoved = entity.alreadyMoved;
        entity.alreadyMoved = true;

        if (entity is InventoryItem && inventory.Contains(entity as InventoryItem)) {
            return false;
        }

        var solidItem = curNode.entities.Find(x => x != entity &&  x.isSolid == true);
        if (solidItem != null) {
            if (solidItem.movementDirection == Entity.Direction.None || solidItem.alreadyMoved || 
                !AttemptValidMove(solidItem, solidItem.GetNextPosition())) {
                entity.OnCollision(solidItem);
                solidItem.OnCollision(entity);

                if (!hasMoved) {
                    entity.OnMoveComplete();
                }
                
                return false;
            } 
        }

        //DOMove
        curNode.MoveEntityToNode(entity);
        entity.OnMoveComplete();

        return true;
    }

    public void Remove(Entity collidingEntity, bool doImmediate = false) {
        if (!pendingDeletions.Contains(collidingEntity)) {
            pendingDeletions.Add(collidingEntity);
        }

        if (doImmediate) {
            ClearPendingDeletions();
        }
    }
    
    public void AddInventoryItem(InventoryItem newItem) {
        inventory.Add(newItem);
        newItem.currentNode?.entities.Remove(newItem);
        newItem.transform.SetParent(InventoryPanel, false);
    }
}
