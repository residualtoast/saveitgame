﻿using System;

public class InventoryItem :Entity
{

    public bool isCollected = false;

    public override void OnCollision(Entity collidingEntity) {
        base.OnCollision(collidingEntity);

        if (collidingEntity is Character) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey($"FIND_{GetName().ToUpper()}"));
            isCollected = true;
        }
    }

    public override void OnMoveComplete() {
        base.OnMoveComplete();
        if (isCollected && !MapManager.Instance.inventory.Contains(this)) {
            MapManager.Instance.AddInventoryItem(this);
        } 
    }

    public virtual MapManager.InputMode SelectItem() {
        MapManager.Instance.DisplayText(LocText.GetLocForKey("BAD_ACTION"));
        return MapManager.InputMode.Movement;
    }

    public virtual void SelectDirection(Direction direction) {

    }
}