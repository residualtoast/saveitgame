﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Torch : InventoryItem
{
    bool isLit = false;
    public Sprite litImage;


    public override MapManager.InputMode SelectItem() {

        var currentPosition = MapManager.Instance.character.currentNode.position;
        var rightNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x + 1, currentPosition.y));
        var leftNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x - 1, currentPosition.y));
        var upNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x, currentPosition.y - 1));
        var downNode = MapManager.Instance.GetNodeAtPosition(new Vector2Int(currentPosition.x, currentPosition.y + 1));

        if (!isLit) {
            if ((rightNode != null && rightNode.entities.Exists(x => x is Fire)) ||
                    (leftNode != null && leftNode.entities.Exists(x => x is Fire)) ||
                    (upNode != null && upNode.entities.Exists(x => x is Fire)) ||
                    (downNode != null && downNode.entities.Exists(x => x is Fire))) {
                GetComponent<Image>().sprite = litImage;

                MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_UNLIT_TORCH"));
                isLit = true;
            }
        } else {
            if (rightNode != null && rightNode.entities.Exists(x => x is Vines)){
                MapManager.Instance.Spawn("Fire", rightNode.position);
                MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_LIT_TORCH"));
            }
            if (leftNode != null && leftNode.entities.Exists(x => x is Vines)) {
                MapManager.Instance.Spawn("Fire", leftNode.position);
                MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_LIT_TORCH"));
            }

            if (upNode != null && upNode.entities.Exists(x => x is Vines)) {
                MapManager.Instance.Spawn("Fire", upNode.position);
                MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_LIT_TORCH"));
            }

            if (downNode != null && downNode.entities.Exists(x => x is Vines)) {
                MapManager.Instance.Spawn("Fire", downNode.position);
                MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_LIT_TORCH"));
            }
           
        }


        return MapManager.InputMode.Movement;
    }
}
