﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hose : InventoryItem
{
    public override MapManager.InputMode SelectItem() {
        if (MapManager.Instance.character.currentNode.entities.Exists(x => x is Pump)) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_HOSE"));
            MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_PUMP_AND_HOSE"));

            var oldPosition = MapManager.Instance.character.currentNode.position;
            MapManager.Instance.Spawn("FlowingWater", new Vector2Int(oldPosition.x - 1, oldPosition.y - 1));

            return MapManager.InputMode.Movement;
        } else {
            return base.SelectItem();
        }
        
    }
}
