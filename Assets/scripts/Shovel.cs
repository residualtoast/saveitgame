﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shovel : InventoryItem
{

    public override MapManager.InputMode SelectItem() {
        MapManager.Instance.DisplayText(LocText.GetLocForKey("INPUT_DIRECTION_SHOVEL"));
        return MapManager.InputMode.ItemDirection;
    }

    public override void SelectDirection(Direction direction) {
        var newEmbankment = MapManager.Instance.Spawn("Embankment", MapManager.Instance.character.currentNode.position);
        newEmbankment.GetComponent<Embankment>().movementDirection = direction;
        MapManager.Instance.DisplayText(LocText.GetLocForKey("USE_SHOVEL"));
    }
}
