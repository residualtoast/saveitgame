﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigTree : Entity
{

    // Start is called before the first frame update
    protected override void Start() {
        if (isFirstSpawn) {
            base.Start();
            MapManager.Instance.Spawn("TreePart", new Vector2Int(currentNode.position.x + 1, currentNode.position.y));
            MapManager.Instance.Spawn("TreePart", new Vector2Int(currentNode.position.x, currentNode.position.y - 1));
            MapManager.Instance.Spawn("TreePart", new Vector2Int(currentNode.position.x + 1, currentNode.position.y - 1));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
