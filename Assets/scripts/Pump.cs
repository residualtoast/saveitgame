﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pump : Entity
{
    public override void OnCollision(Entity collidingEntity) {
        base.OnCollision(collidingEntity);
        if (collidingEntity is Character) {
            MapManager.Instance.DisplayText(LocText.GetLocForKey($"FIND_{GetName().ToUpper()}"));
        }
    }
}
