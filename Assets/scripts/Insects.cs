﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Insects : Entity
{
    List<Direction> directionQueue = new List<Direction> { Direction.Right, Direction.Right, Direction.Right, Direction.Up, Direction.Up, Direction.Left, Direction.Up, Direction.Up };
    int queuestep;

    // Start is called before the first frame update
    protected override void Start() {
        base.Start();
        queuestep = 0;
        movementDirection = directionQueue[queuestep];
    }

    public override void OnMoveComplete() {
        queuestep = (queuestep + 1) % directionQueue.Count;
        movementDirection = directionQueue[queuestep];
        base.OnMoveComplete();
    }

    public override void OnCollision(Entity collidingEntity)
    {

        if (collidingEntity is BigTree)
        {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("TREE_DEATH_INSECTS"));
            MapManager.Instance.failState = true;
        }
    }

    protected override void OnDestroy() {
        MapManager.Instance?.DisplayText(LocText.GetLocForKey("INSECT_DEATH"));
        base.OnDestroy();
    }
}
