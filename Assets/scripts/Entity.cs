﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    
    public enum Direction
    {
        None,
        Up,
        Down,
        Left,
        Right
    }

    public bool isSolid = true;
    public MapNode currentNode;
    public bool doesMove = true;

    public bool isFirstSpawn = false;
    
    public Direction movementDirection;
    public bool alreadyMoved;

    public int speed = 1;
    protected int speedTicker = 1;

    public int lifetime = 0;
    private int lifetimeTracker = 0;

    public bool isHazard = false;
    public bool canLeave = true;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        if (isFirstSpawn) {
            var spawnTextKey = "SPAWN_" + GetName().ToUpper();
            if (LocText.allText.ContainsKey(spawnTextKey)) {
                MapManager.Instance.DisplayText(LocText.GetLocForKey(spawnTextKey));
            }
        }
    }

    protected virtual void OnDestroy() {
        currentNode?.entities.Remove(this);
    }

    public virtual void OnCollision(Entity collidingEntity) {

    }

    public virtual void OnMoveComplete() {
        speedTicker++;
        if (speedTicker > speed) {
            speedTicker = 1;
        }
        if (lifetime > 0 && lifetimeTracker++ > lifetime) {
            MapManager.Instance.Remove(this);
        }
    }

    public virtual string GetName() {
        return transform.name.Split('(')[0];
    }

    public Vector2Int GetNextPosition() {

        var direction = movementDirection;
        var isLeavingMap = (currentNode.position.x == 0 && direction == Entity.Direction.Left) || (currentNode.position.x == MapManager.Instance.mapWidth - 1 && direction == Entity.Direction.Right) ||
            (currentNode.position.y == 0 && direction == Entity.Direction.Up) || (currentNode.position.y == MapManager.Instance.mapWidth - 1 && direction == Entity.Direction.Down);

        if (isLeavingMap && canLeave) {
            MapManager.Instance.Remove(this);
        }

        if (!doesMove || speedTicker < speed || isLeavingMap) {
            return currentNode.position;
        }

        return new Vector2Int(currentNode.position.x + ((direction == Entity.Direction.Right) ? 1 : ((direction == Entity.Direction.Left) ? -1 : 0)),
            currentNode.position.y + ((direction == Entity.Direction.Down) ? 1 : ((direction == Entity.Direction.Up) ? -1 : 0)));

    }

}
