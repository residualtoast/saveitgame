﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cultist : Entity
{
    List<Direction> directionQueue = new List<Direction> { Direction.Down, Direction.Right, Direction.Up, Direction.Right, Direction.Down, Direction.Right };
    int queuestep;


    // Start is called before the first frame update
    protected override void Start() {
        base.Start();
        queuestep = 0;
        movementDirection = Direction.Right;
    }

    public override void OnCollision(Entity collidingEntity)
    {
        

        if (collidingEntity is Character)
        {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("PLAYER_DEATH_CULTIST"));
            MapManager.Instance.failState = true;
        } else if (collidingEntity is BigTree)
        {
            MapManager.Instance.DisplayText(LocText.GetLocForKey("TREE_DEATH_CULTIST"));
            MapManager.Instance.failState = true;
        } else {
            movementDirection = directionQueue[queuestep];
            queuestep = (queuestep + 1) % directionQueue.Count;
        }
    }
}
